FROM python:3.7

COPY requirements.txt /app/

RUN pip install -r /app/requirements.txt

CMD ["rm", "-rf", "app"]

CMD ["jupyter", "nbextension", "enable", "--py", "widgetsnbextension"]

CMD ["jupyter",  "labextension", "install", "@pyviz/jupyterlab_pyviz"]

CMD ["jupyter-nbextension", "install", "rise", "--py", "--sys-prefix"]

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get -y update
RUN apt-get install -y google-chrome-stable

# install chromedriver
RUN apt-get install -yqq unzip
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

# Run jupyter notebook
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--notebook-dir=/home/app", "--allow-root"]
